# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from pybuild import Pybuild1
from pybuild.info import PN, PV, P

from common.exec import Exec


class Package(Exec, Pybuild1):
    NAME = "Portmod OpenMW Config Module"
    DESC = "Sorts openmw.cfg and settings.cfg to match mods installed by portmod."
    LICENSE = "GPL-3"
    KEYWORDS = "openmw tes3mp"
    HOMEPAGE = f"https://gitlab.com/portmod/{PN}"
    SRC_URI = f"https://gitlab.com/portmod/{PN}/-/archive/{PV}/{P}.tar.gz"
    PROPERTIES = "module"
    S = f"{P}/{P}"
    IUSE = "grass"

    def src_prepare(self):
        # The python module must be in the root directory, so we flatten its contents
        # so that we install both the documentation and the module even though they are
        # stored separately
        for file in os.listdir("openmw_config"):
            os.rename(os.path.join("openmw_config", file), file)

        if "grass" in self.USE:
            self.SETTINGS = {
                "Groundcover": {
                    "enabled": "true",
                    "density": 0.5,
                    "min chunk size": 0.5,
                }
            }
