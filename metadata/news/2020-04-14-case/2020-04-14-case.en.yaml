Title: Case Insensitive Installation
Author: Benjamin Winger <bmw@disroot.org>
Posted: 2020-04-14
Revision: 1
News-Item-Format: 2.0
Body: Portmod now supports case insensitive installation, meaning that mods that
  contain multiple files, with different case (e.g. "Textures" and "textures") that
  are designed to overwrite each other (e.g. optional directories that partially
  overwrite core files), now overwrite each other properly, rather than installing
  both versions side by side.

  While it is likely that most mods do not need to be reinstalled, we recommend
  that you re-install all your mods to ensure that any installations that need
  to be fixed are fixed.

  Alternatively, if you want to be more precise and avoid unnecessary time spent
  reinstalling mods that don't need to be reinstalled, you can use the following
  command to identify files within mod installations which are affected by this
  problem.


  `find DIR |sed 's,\(.*\)/\(.*\)$,\1/\2\t\1/\L\2,'|sort|uniq -D -f 1|cut -f 1`


  Where DIR is the location of your mod installation directory
  (on Linux, ~/.local/share/portmod/mods).
  Note that mod's name and category are part of the installation path, so they will
  show up as part of the output, and if you want to avoid adding those mods to your
  world file you should use the `--oneshot` argument when invoking omwmerge.


  If you use a case-insensitive filesystem, or an OS such as Windows that treats all
  filesystems as case insensitive, then this does not apply to you.
